/*
Author:		Matthew Hollands
Date:		03/12/2018
Server:		d-ovs16-dwh-01
Database:	Levelisation

This copies all of the readings from the Microtricity2 database to the Levelisation database.

*/


-- This Command clears down the Readings table in the Levelisation database.  This is not currently working as RPC is not turned on on d-ovs
TRUNCATE TABLE dbo.READINGS_STAGING;

-- Copy the Readings from the Microtricity database into the Levelisation database
INSERT INTO [dbo].[READINGS_STAGING]
(
    READ_ID,
    INSTALLATION_ID_link,
    Meter_Read_Type,
    Meter_Read_Date,
    Meter_Read_Reason,
    Generation_Read,
    Export_Read,
    IncludedInLevelisation,
    Gen_Validation,
    Modified_By,
    Date_Modified,
    Time_Modified,
    Claimed,
    Generation_Clocked,
    Export_Clocked,
    Notes,
    MSN,
    ESP_Billed,
    Recieved_Method,
    Photo,
    Export_MSN,
    Current_Price_Initial,
    Current_Price_Ext1,
    Current_Price_Ext2,
    Current_Price_Export,
    Current_Price_Export1,
    Current_Price_Export2,
    Current_Price_InitExp2,
    Fit_Period
)
SELECT R.READ_ID,
       R.INSTALLATION_ID_link,
       R.Meter_Read_Type,
       R.Meter_Read_Date,
       R.Meter_Read_Reason,
       R.Generation_Read,
       R.Export_Read,
       R.IncludedInLevelisation,
       R.Gen_Validation,
       R.Modified_By,
       R.Date_Modified,
       R.Time_Modified,
       R.Claimed,
       R.Generation_Clocked,
       R.Export_Clocked,
       R.Notes,
       R.MSN,
       R.ESP_Billed,
       R.Recieved_Method,
       R.Photo,
       R.Export_MSN,
       Initial_Price.Price AS Current_Price_Initial,
       Ext1_Price.Price AS Current_Price_Ext1,
       Ext2_Price.Price AS Current_Price_Ext2,
       Export_Price.Price AS Current_Price_Export,
       Export1_Price.Price AS Current_Price_Export1,
       Export2_Price.Price AS Current_Price_Export2,
       InitExp2_Price.Price AS Current_Price_InitExp2,
       Initial_Price.FiT_Period
FROM [UH-GENDB-01].[Microtricity2].[dbo].[READINGS] R
INNER JOIN [UH-GENDB-01].[Microtricity2].[dbo].[INSTALLATION] I ON R.INSTALLATION_ID_link = I.INSTALLATION_ID 
LEFT JOIN dbo.RATES Initial_Price ON I.Initial_System_Tariff_Rate = Initial_Price.Tariff_Code AND R.Meter_Read_Date BETWEEN Initial_Price.Valid_From AND Initial_Price.Valid_To
LEFT JOIN dbo.RATES Export_Price ON I.Initial_System_Export_Rate = Export_Price.Tariff_Code AND R.Meter_Read_Date BETWEEN Export_Price.Valid_From AND Export_Price.Valid_To
LEFT JOIN dbo.RATES Ext1_Price ON I.Extension_1_Tariff_Rate = Ext1_Price.Tariff_Code AND R.Meter_Read_Date BETWEEN Ext1_Price.Valid_From AND Ext1_Price.Valid_To
LEFT JOIN dbo.RATES Ext2_Price ON I.Extension_2_Tariff_Rate = Ext2_Price.Tariff_Code AND R.Meter_Read_Date BETWEEN Ext2_Price.Valid_From AND Ext2_Price.Valid_To
LEFT JOIN dbo.RATES Export1_Price ON I.Extension_1_Export_Rate = Export1_Price.Tariff_Code AND R.Meter_Read_Date BETWEEN Export1_Price.Valid_From AND Export1_Price.Valid_To
LEFT JOIN dbo.RATES Export2_Price ON I.Extension_2_Export_Rate = Export2_Price.Tariff_Code AND R.Meter_Read_Date BETWEEN Export2_Price.Valid_From AND Export2_Price.Valid_To
LEFT JOIN dbo.RATES InitExp2_Price ON I.InitSysExpRate2 = InitExp2_Price.Tariff_Code AND R.Meter_Read_Date BETWEEN InitExp2_Price.Valid_From AND InitExp2_Price.Valid_To;
