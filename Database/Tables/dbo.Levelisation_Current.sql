CREATE TABLE [dbo].[Levelisation_Current]
(
[INSTALLATION_ID] [int] NULL,
[FiT_ID_Initial] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[FiT_ID_Ext1] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[FiT_ID_Ext2] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Meter_Read_Reason_Start] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Meter_Read_Date_START] [datetime] NULL,
[Meter_Read_Reason_END] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Meter_Read_Date_END] [datetime] NULL,
[Units_Exported] [int] NULL,
[Units_Generated] [int] NULL,
[Generation_Payment] [float] NULL,
[Export_Payment] [numeric] (27, 6) NULL,
[Total_Payment] [float] NULL,
[Number_of_Splits] [int] NULL
) ON [PRIMARY]
GO
