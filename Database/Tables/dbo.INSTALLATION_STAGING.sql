CREATE TABLE [dbo].[INSTALLATION_STAGING]
(
[INSTALLATION_ID] [int] NULL,
[Scheme_Type] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL CONSTRAINT [DF__INSTALLAT__Schem__5441852A] DEFAULT ('FiT'),
[Generation_Type] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Total_Installed_Capacity_kW] [float] NULL,
[FiT_ID_Initial] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Initial_System_Tariff_Rate] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Initial_System_Export_Rate] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Initial_System_Pct_Split] [float] NULL CONSTRAINT [DF__INSTALLAT__Initi__5535A963] DEFAULT ((1)),
[FiT_ID_Ext1] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Extension_1_Tariff_Rate] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Extension_1_Export_Rate] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Extension_1_Pct_Split] [float] NULL CONSTRAINT [DF__INSTALLAT__Exten__5629CD9C] DEFAULT ((0)),
[FiT_ID_Ext2] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Extension_2_Tariff_Rate] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Extension_2_Export_Rate] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Extension_2_Pct_Split] [float] NULL CONSTRAINT [DF__INSTALLAT__Exten__571DF1D5] DEFAULT ((0)),
[TopUp_Tariff_Rate] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[ExpPct1] [float] NULL,
[ExpPct2] [float] NULL,
[InitSysExpRate2] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Account_Status] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
