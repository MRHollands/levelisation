CREATE TABLE [dbo].[Lev_Calcs]
(
[FiT_ID_Initial] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Meter_Read_Reason_Old] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Meter_Read_Date_Old] [date] NULL,
[Meter_Read_Reason_New] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Meter_Read_Date_New] [date] NULL,
[Generation] [int] NULL,
[Old_Price] [numeric] (12, 2) NULL,
[New_Price] [numeric] (12, 2) NULL,
[Installation_ID] [int] NULL
) ON [PRIMARY]
GO
