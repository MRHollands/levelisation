CREATE TABLE [dbo].[RATES]
(
[RATE_ID] [int] NULL,
[Generation_Type] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Tariff_Code] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Description] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[FiT_Period] [int] NULL,
[Min_Capacity] [int] NULL,
[Max_Capacity] [int] NULL,
[Valid_From] [datetime] NULL,
[Valid_To] [datetime] NULL,
[Price] [numeric] (12, 2) NULL,
[EligibilityDateFrom] [datetime] NULL,
[EligibilityDateTo] [datetime] NULL,
[ESP_Year] [int] NULL,
[Gross_Electricity_Charge] [numeric] (12, 2) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RATES] ADD CONSTRAINT [UQ_codes] UNIQUE NONCLUSTERED  ([Tariff_Code], [FiT_Period], [Valid_From], [Valid_To]) ON [PRIMARY]
GO
