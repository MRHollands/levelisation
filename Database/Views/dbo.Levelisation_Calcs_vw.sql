SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE VIEW [dbo].[Levelisation_Calcs_vw]
AS
/*
Author:		Matthew Hollands
Date:		29/11/2018
Server:		d-ovs16-dwh-01
Database:	Levelisation

This is the beginning of the development of an SQL levelisation calculation solution.  Currently it is calculating the Lev Totals for the most simple scenario:  No Split, No Extensions
*/

SELECT 
I.INSTALLATION_ID,
I.FiT_ID_Initial,
I.FiT_ID_Ext1,
I.FiT_ID_Ext2,
C.Meter_Read_Reason_Start,
C.Meter_Read_Date_START,
C.Meter_Read_Reason_END,
C.Meter_Read_Date_END,
C.Units_Exported,
C.Units_Generated,
C.Generation_Payment,
C.Export_Payment,
C.Generation_Payment + C.Export_Payment AS Total_Payment,
C.Number_of_Splits 

FROM dbo.INSTALLATION_STAGING I 
INNER JOIN dbo.Initial_Calcs_vw C ON I.INSTALLATION_ID = C.INSTALLATION_ID

WHERE
I.Account_Status = 'Registered Live'

GO
