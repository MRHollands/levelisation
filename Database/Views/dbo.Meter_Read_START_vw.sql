SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[Meter_Read_START_vw]
AS
/*
Author:		Matthew Hollands
Date:		05/12/2018
Server:		d-ovs16-dwh-01
Database:	Levelisation

I've written this as a view, but it may just need to be a part of the SP that populates the Lev calcs table.  This view brings back all the Start reads.  
Normal calcs will just have one Start read, but Meter Exchange, Change of Tennancy or Split Reads will have more than one.
*/

-- CTE to find Normal Start Reads.
WITH MaxOfPaidToRead
AS (
   SELECT INSTALLATION_ID_link,
          MAX(Meter_Read_Date) AS MaxOfMeter_Read_Date,
          IncludedInLevelisation
   FROM dbo.Readings_Staging
   WHERE IncludedInLevelisation = 'True'
   GROUP BY INSTALLATION_ID_link,
            IncludedInLevelisation),

     -- CTE to find Meter Exchange Start Reads.
     MaxOfPaidToRead_MX
AS (SELECT INSTALLATION_ID_link,
           Meter_Read_Date AS MaxOfMeter_Read_Date,
           IncludedInLevelisation
    FROM dbo.Readings_Staging
    WHERE IncludedInLevelisation = 'FALSE'
          AND Meter_Read_Reason IN ( 'Initial (MX)' )),

     -- CTE to find COT Start Reads.
     MaxOfPaidToRead_COT
AS (SELECT INSTALLATION_ID_link,
           Meter_Read_Date AS MaxOfMeter_Read_Date,
           IncludedInLevelisation
    FROM dbo.Readings_Staging
    WHERE IncludedInLevelisation = 'FALSE'
          AND Meter_Read_Reason IN ( 'Initial (COT)' )),

     -- CTE to find Split Start Reads.  This is the big one.  Still need to work out how to do this.  It's not currently doing anything.
     MaxOfPaidToRead_Split
AS (SELECT INSTALLATION_ID_link,
           Meter_Read_Date AS MaxOfMeter_Read_Date,
           IncludedInLevelisation
    FROM dbo.Readings_Staging
    WHERE IncludedInLevelisation = 'FALSE')
	
-- This selects the Normal Start reads
SELECT READINGS_1.READ_ID,
       READINGS_1.INSTALLATION_ID_link,
       READINGS_1.Meter_Read_Type,
       READINGS_1.Meter_Read_Date AS Meter_Read_Date_START,
       READINGS_1.Meter_Read_Reason,
       READINGS_1.Generation_Read AS Generation_Read_START,
       READINGS_1.Export_Read AS Export_Read_START,
       READINGS_1.IncludedInLevelisation,
       READINGS_1.Gen_Validation,
       READINGS_1.Modified_By,
       READINGS_1.Date_Modified,
       READINGS_1.Time_Modified,
       READINGS_1.Claimed,
       READINGS_1.MSN,
       READINGS_1.Fit_Period,
       READINGS_1.Current_Price_Initial,
       READINGS_1.Current_Price_Export,
       READINGS_1.Current_Price_Ext1,
       READINGS_1.Current_Price_Ext2,
       READINGS_1.Current_Price_Export1,
       READINGS_1.Current_Price_Export2,
       READINGS_1.Current_Price_InitExp2

FROM dbo.Readings_Staging AS READINGS_1
    INNER JOIN MaxOfPaidToRead AS MaxOfPaidToRead_1
        ON READINGS_1.INSTALLATION_ID_link = MaxOfPaidToRead_1.INSTALLATION_ID_link
           AND READINGS_1.Meter_Read_Date = MaxOfPaidToRead_1.MaxOfMeter_Read_Date
           AND READINGS_1.IncludedInLevelisation = MaxOfPaidToRead_1.IncludedInLevelisation
UNION

-- This selects the Meter Exchange Start reads
SELECT READINGS_1.READ_ID,
       READINGS_1.INSTALLATION_ID_link,
       READINGS_1.Meter_Read_Type,
       READINGS_1.Meter_Read_Date AS Meter_Read_Date_START,
       READINGS_1.Meter_Read_Reason,
       READINGS_1.Generation_Read AS Generation_Read_START,
       READINGS_1.Export_Read AS Export_Read_START,
       READINGS_1.IncludedInLevelisation,
       READINGS_1.Gen_Validation,
       READINGS_1.Modified_By,
       READINGS_1.Date_Modified,
       READINGS_1.Time_Modified,
       READINGS_1.Claimed,
       READINGS_1.MSN,
       READINGS_1.Fit_Period,
       READINGS_1.Current_Price_Initial,
       READINGS_1.Current_Price_Export,
       READINGS_1.Current_Price_Ext1,
       READINGS_1.Current_Price_Ext2,
       READINGS_1.Current_Price_Export1,
       READINGS_1.Current_Price_Export2,
       READINGS_1.Current_Price_InitExp2

FROM dbo.Readings_Staging AS READINGS_1
    INNER JOIN MaxOfPaidToRead_MX AS MaxOfPaidToRead_MX
        ON READINGS_1.INSTALLATION_ID_link = MaxOfPaidToRead_MX.INSTALLATION_ID_link
           AND READINGS_1.Meter_Read_Date = MaxOfPaidToRead_MX.MaxOfMeter_Read_Date
           AND READINGS_1.IncludedInLevelisation = MaxOfPaidToRead_MX.IncludedInLevelisation
UNION

-- This selects the COT Start reads
SELECT READINGS_1.READ_ID,
       READINGS_1.INSTALLATION_ID_link,
       READINGS_1.Meter_Read_Type,
       READINGS_1.Meter_Read_Date AS Meter_Read_Date_START,
       READINGS_1.Meter_Read_Reason,
       READINGS_1.Generation_Read AS Generation_Read_START,
       READINGS_1.Export_Read AS Export_Read_START,
       READINGS_1.IncludedInLevelisation,
       READINGS_1.Gen_Validation,
       READINGS_1.Modified_By,
       READINGS_1.Date_Modified,
       READINGS_1.Time_Modified,
       READINGS_1.Claimed,
       READINGS_1.MSN,
       READINGS_1.Fit_Period,
       READINGS_1.Current_Price_Initial,
       READINGS_1.Current_Price_Export,
	   READINGS_1.Current_Price_Ext1,
       READINGS_1.Current_Price_Ext2,
       READINGS_1.Current_Price_Export1,
       READINGS_1.Current_Price_Export2,
       READINGS_1.Current_Price_InitExp2

FROM dbo.Readings_Staging AS READINGS_1
    INNER JOIN MaxOfPaidToRead_COT AS MaxOfPaidToRead_COT
        ON READINGS_1.INSTALLATION_ID_link = MaxOfPaidToRead_COT.INSTALLATION_ID_link
           AND READINGS_1.Meter_Read_Date = MaxOfPaidToRead_COT.MaxOfMeter_Read_Date
          AND READINGS_1.IncludedInLevelisation = MaxOfPaidToRead_COT.IncludedInLevelisation
/*
UNION

-- This selects the Split Start reads
SELECT READINGS_1.READ_ID,
       READINGS_1.INSTALLATION_ID_link,
       READINGS_1.Meter_Read_Type,
       READINGS_1.Meter_Read_Date AS Meter_Read_Date_START,
       READINGS_1.Meter_Read_Reason,
       READINGS_1.Generation_Read AS Generation_Read_START,
       READINGS_1.Export_Read AS Export_Read_START,
       READINGS_1.IncludedInLevelisation,
       READINGS_1.Gen_Validation,
       READINGS_1.Modified_By,
       READINGS_1.Date_Modified,
       READINGS_1.Time_Modified,
       READINGS_1.Claimed,
       READINGS_1.MSN,
       READINGS_1.Fit_Period,
       READINGS_1.Current_Price_Initial,
       READINGS_1.Current_Price_Export,
	   READINGS_1.Current_Price_Ext1,
       READINGS_1.Current_Price_Ext2,
       READINGS_1.Current_Price_Export1,
       READINGS_1.Current_Price_Export2,
       READINGS_1.Current_Price_InitExp2

FROM dbo.Readings_Staging AS READINGS_1
    INNER JOIN MaxOfPaidToRead_Split AS MaxOfPaidToRead_Split
        ON READINGS_1.INSTALLATION_ID_link = MaxOfPaidToRead_Split.INSTALLATION_ID_link
           AND READINGS_1.Meter_Read_Date = MaxOfPaidToRead_Split.MaxOfMeter_Read_Date
           AND READINGS_1.IncludedInLevelisation = MaxOfPaidToRead_Split.IncludedInLevelisation
		   */
		   ;


GO
