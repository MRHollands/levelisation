SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[Meter_Read_END_vw]
AS
WITH
/*
Author:		Matthew Hollands
Date:		05/12/2018
Server:		d-ovs16-dwh-01
Database:	Levelisation

I've written this as a view, but it may just need to be a part of the SP that populates the Lev calcs table.  This view brings back all the End reads.  
Normal calcs will just have one End read, but Meter Exchange, Change of Tennancy or Split Reads will have more than one.


*/

-- CTE for Normal End Reads
MaxOfPaidToRead
AS (
   SELECT INSTALLATION_ID_link,
          MAX(Meter_Read_Date) AS MaxOfMeter_Read_Date,
          IncludedInLevelisation
   FROM dbo.Readings_Staging
   WHERE (IncludedInLevelisation = 'False')
         -- 2 Year Audit and Interim reads are not used in Levelisation and Initial MX reads cannot ever be an End(New) read.  
         AND (Meter_Read_Reason NOT IN ( '2 Year Audit', 'Interim', 'Initial (MX)' ))
   GROUP BY INSTALLATION_ID_link,
            IncludedInLevelisation),

-- CTE for Meter Exchange End Reads
MaxOfPaidToRead_MX
AS (SELECT INSTALLATION_ID_link,
           Meter_Read_Date AS MaxOfMeter_Read_Date,
           IncludedInLevelisation
    FROM dbo.Readings_Staging
    WHERE (IncludedInLevelisation = 'False')
          AND (Meter_Read_Reason = 'Final (MX)')),

-- CTE for COT End Reads
MaxOfPaidToRead_COT
AS (SELECT INSTALLATION_ID_link,
           Meter_Read_Date AS MaxOfMeter_Read_Date,
           IncludedInLevelisation
    FROM dbo.Readings_Staging
    WHERE (IncludedInLevelisation = 'False')
          AND (Meter_Read_Reason = 'Final (COT)'))

-- This bit adds the normal End Reads
SELECT READINGS_1.READ_ID,
       READINGS_1.INSTALLATION_ID_link,
       READINGS_1.Meter_Read_Type,
       READINGS_1.Meter_Read_Date AS Meter_Read_Date_END,
       READINGS_1.Meter_Read_Reason,
       READINGS_1.Generation_Read AS Generation_Read_END,
       READINGS_1.Export_Read AS Export_Read_END,
       READINGS_1.IncludedInLevelisation,
       READINGS_1.Gen_Validation,
       READINGS_1.Modified_By,
       READINGS_1.Date_Modified,
       READINGS_1.Time_Modified,
       READINGS_1.Claimed,
       READINGS_1.MSN,
       READINGS_1.Fit_Period,
       READINGS_1.Current_Price_Initial,
       READINGS_1.Current_Price_Export,
       READINGS_1.Current_Price_Ext1,
       READINGS_1.Current_Price_Ext2,
       READINGS_1.Current_Price_Export1,
       READINGS_1.Current_Price_Export2,
       READINGS_1.Current_Price_InitExp2


FROM dbo.Readings_Staging AS READINGS_1
    INNER JOIN MaxOfPaidToRead AS MaxOfPaidToRead_1
        ON READINGS_1.INSTALLATION_ID_link = MaxOfPaidToRead_1.INSTALLATION_ID_link
           AND READINGS_1.Meter_Read_Date = MaxOfPaidToRead_1.MaxOfMeter_Read_Date
           AND READINGS_1.IncludedInLevelisation = MaxOfPaidToRead_1.IncludedInLevelisation
WHERE READINGS_1.Meter_Read_Reason NOT IN ( '2 Year Audit', 'Interim', 'Initial (MX)' )
UNION

--  This bit adds the Meter Exchange End Reads 
SELECT READINGS_1.READ_ID,
       READINGS_1.INSTALLATION_ID_link,
       READINGS_1.Meter_Read_Type,
       READINGS_1.Meter_Read_Date AS Meter_Read_Date_END,
       READINGS_1.Meter_Read_Reason,
       READINGS_1.Generation_Read AS Generation_Read_END,
       READINGS_1.Export_Read AS Export_Read_END,
       READINGS_1.IncludedInLevelisation,
       READINGS_1.Gen_Validation,
       READINGS_1.Modified_By,
       READINGS_1.Date_Modified,
       READINGS_1.Time_Modified,
       READINGS_1.Claimed,
       READINGS_1.MSN,
       READINGS_1.Fit_Period,
       READINGS_1.Current_Price_Initial,
       READINGS_1.Current_Price_Export,
	   READINGS_1.Current_Price_Ext1,
       READINGS_1.Current_Price_Ext2,
       READINGS_1.Current_Price_Export1,
       READINGS_1.Current_Price_Export2,
       READINGS_1.Current_Price_InitExp2

FROM dbo.Readings_Staging AS READINGS_1
    INNER JOIN MaxOfPaidToRead_MX AS MaxOfPaidToRead_MX
        ON READINGS_1.INSTALLATION_ID_link = MaxOfPaidToRead_MX.INSTALLATION_ID_link
           AND READINGS_1.Meter_Read_Date = MaxOfPaidToRead_MX.MaxOfMeter_Read_Date
           AND READINGS_1.IncludedInLevelisation = MaxOfPaidToRead_MX.IncludedInLevelisation
WHERE READINGS_1.Meter_Read_Reason NOT IN ( '2 Year Audit', 'Interim', 'Initial (MX)' )
UNION

--  This bit adds the COT End Reads 
SELECT READINGS_1.READ_ID,
       READINGS_1.INSTALLATION_ID_link,
       READINGS_1.Meter_Read_Type,
       READINGS_1.Meter_Read_Date AS Meter_Read_Date_END,
       READINGS_1.Meter_Read_Reason,
       READINGS_1.Generation_Read AS Generation_Read_END,
       READINGS_1.Export_Read AS Export_Read_END,
       READINGS_1.IncludedInLevelisation,
       READINGS_1.Gen_Validation,
       READINGS_1.Modified_By,
       READINGS_1.Date_Modified,
       READINGS_1.Time_Modified,
       READINGS_1.Claimed,
       READINGS_1.MSN,
       READINGS_1.Fit_Period,
       READINGS_1.Current_Price_Initial,
       READINGS_1.Current_Price_Export,
	   READINGS_1.Current_Price_Ext1,
       READINGS_1.Current_Price_Ext2,
       READINGS_1.Current_Price_Export1,
       READINGS_1.Current_Price_Export2,
       READINGS_1.Current_Price_InitExp2

FROM dbo.Readings_Staging AS READINGS_1
    INNER JOIN MaxOfPaidToRead_COT AS MaxOfPaidToRead_COT
        ON READINGS_1.INSTALLATION_ID_link = MaxOfPaidToRead_COT.INSTALLATION_ID_link
           AND READINGS_1.Meter_Read_Date = MaxOfPaidToRead_COT.MaxOfMeter_Read_Date
           AND READINGS_1.IncludedInLevelisation = MaxOfPaidToRead_COT.IncludedInLevelisation
WHERE READINGS_1.Meter_Read_Reason NOT IN ( '2 Year Audit', 'Interim', 'Initial (MX)' );



GO
