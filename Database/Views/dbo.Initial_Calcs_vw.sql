SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[Initial_Calcs_vw]
AS
/*
Author:		Matthew Hollands
Date:		05/12/2018
Server:		d-ovs16-dwh-01
Database:	Levelisation

The Calculation Engine for Levelisation.  The most important part of the process.
*/

SELECT 
I.INSTALLATION_ID,
Start_Read.Meter_Read_Reason AS Meter_Read_Reason_Start,
Start_Read.Meter_Read_Date_Start,
End_Read.Meter_Read_Reason AS Meter_Read_Reason_End,
End_Read.Meter_Read_Date_End,
CASE
WHEN End_read.Export_Read_END - Start_read.Export_Read_Start IS NULL THEN ((End_read.Generation_read_End - Start_read.Generation_read_Start) / 2)
ELSE End_read.Export_Read_END - Start_read.Export_Read_Start
END AS Units_Exported,
End_read.Generation_read_End - Start_read.Generation_read_Start AS Units_Generated,
((End_read.Generation_read_End - Start_read.Generation_read_Start) * (ISNULL(End_Read.Current_Price_Initial,0)/100)*ISNULL(I.Initial_System_Pct_Split,0)) +
((End_read.Generation_read_End - Start_read.Generation_read_Start) * (ISNULL(End_Read.Current_Price_Ext1,0)/100)*ISNULL(I.Extension_1_Pct_Split,0)) +
((End_read.Generation_read_End - Start_read.Generation_read_Start) * (ISNULL(End_Read.Current_Price_Ext2,0)/100)*ISNULL(I.Extension_2_Pct_Split,0))  
AS Generation_Payment,
CASE
WHEN End_read.Export_Read_END - Start_read.Export_Read_Start IS NULL THEN ((End_read.Generation_read_End - Start_read.Generation_read_Start) / 2) * (End_Read.Current_Price_Initial/100)
ELSE (End_read.Export_Read_END - Start_read.Export_Read_Start) * (End_Read.Current_Price_Export/100)
END AS Export_Payment,
End_read.fit_period - Start_read.fit_period AS Number_of_Splits

FROM installation_staging I 
LEFT JOIN meter_read_start_vw Start_Read ON I.installation_id = Start_read.installation_id_link
LEFT JOIN meter_read_end_vw End_Read ON I.installation_id = End_read.installation_id_link;


GO
