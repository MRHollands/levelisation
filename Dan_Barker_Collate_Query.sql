BEGIN

    SET NOCOUNT ON;

    BEGIN TRY 
    
        IF OBJECT_ID('tempdb..#MyTempTest') IS NOT NULL 
        DROP TABLE #MyTempTest;

        SELECT dimins.anlage,dimins.vertrag,dimcon.vkonto,dimcac.GPart 
                ,DimIns.RowIsCurrent as DimInsRowIsCurrent
                ,DimCon.RowIsCurrent as DimConRowIsCurrent
                ,DimCAc.RowIsCurrent as DimCAcRowIsCurrent
                ,DimBPa.RowIsCurrent as DimBPaRowIsCurrent
                ,DimIns.RowStartDate as DimInsRowStartDate
                ,DimCon.RowStartDate as DimConRowStartDate
                ,DimCAc.RowStartDate as DimCAcRowStartDate
                ,DimIns.RowEndDate as DimInsRowEndDate
                ,DimBPa.RowStartDate as DimBPaRowStartDate
                ,DimCon.RowEndDate as DimConRowEndDate
                ,DimCAc.RowEndDate as DimCAcRowEndDate
                ,DimBPa.RowEndDate as DimBPaRowEndDate  
        into #MyTempTest

        from bas.DimInstallation as DimIns

                LEFT JOIN bas.DimContract as DimCon
                ON DimIns.vertrag=DimCon.vertrag
                AND DimIns.RowStartDate < = DimCon.rowEndDate
                AND DimIns.RowEndDate >= DimCon.RowStartDate
                --AND DimCon.RowStartDate <> DimCon.RowEndDate

            LEFT JOIN bas.DimContractAccount as DimCAc
                ON DimCon.vkonto=DimCAc.vkont
                AND DimCon.RowStartDate < = DimCAc.rowEndDate
                AND DimCon.RowEndDate >= DimCAc.RowStartDate
                --AND DimCAc.RowStartDate <> DimCAc.RowEndDate

            LEFT JOIN bas.DimBusinessPartner as DimBPa
                ON DimCAc.[GPart]=DimBPa.[partner]
                AND DimCAc.RowStartDate < = DimBPa.rowEndDate
                AND DimCAc.RowEndDate >= DimBPa.RowStartDate
                --AND DimBPa.RowStartDate <> DimBPa.RowEndDate

                where 
                    DimIns.RowStartDate <> DimIns.RowEndDate
                AND (DimCon.RowStartDate <> DimCon.RowEndDate OR DimCon.rowstartdate is null)
                AND (DimCAc.RowStartDate <> DimCAc.RowEndDate OR DimCAC.RowStartDAte is null)
                AND (DimBPa.RowStartDate <> DimBPa.RowEndDate Or DimBPA.RowStartDate is null)
                --and DimIns.vertrag='0052586998'

        IF OBJECT_ID('tempdb..#all_dates2') IS NOT NULL 
        DROP TABLE #all_dates2;

        SELECT y.* into #all_dates2 from (
                 SELECT
           [anlage], [vertrag],[vkonto],[gpart], DimInsRowStartDate AS "date", 0 AS isEnd FROM #MyTempTest
          UNION
          SELECT 
          [anlage],[vertrag],[vkonto],[gpart], DimInsRowEndDate, 1 FROM #MyTempTest
            UNION
             SELECT
           [anlage], [vertrag],[vkonto],[gpart], DimConRowStartDate AS "date", 0 AS isEnd FROM #MyTempTest
          UNION
          SELECT 
          [anlage],[vertrag],[vkonto],[gpart], DimConRowEndDate, 1 FROM #MyTempTest
            UNION
             SELECT
          [anlage],[vertrag],[vkonto],[gpart], DimCAcRowStartDate AS "date", 0 AS isEnd FROM #MyTempTest
          UNION
          SELECT
           [anlage], [vertrag],[vkonto],[gpart], DimCAcRowEndDate, 1 FROM #MyTempTest
          UNION
           SELECT
            [anlage],[vertrag],[vkonto],[gpart], DimBPaRowStartDate AS "date", 0 AS isEnd FROM #MyTempTest
          UNION
          SELECT 
           [anlage],[vertrag],[vkonto],[gpart], DimBPaRowEndDate, 1 FROM #MyTempTest

        ) as y


        IF OBJECT_ID('tempdb..#all_paired_dates2') IS NOT NULL  
        DROP TABLE #all_paired_dates2;
        SELECT yy.* into #all_paired_dates2 from (
          SELECT  --placeholder, 
           [anlage],[vertrag],[vkonto],[gpart], "date" AS [RowStartDate],
                 LEAD( "date" ) OVER ( PARTITION BY  anlage,[vertrag],[vkonto],[gpart] ORDER BY  "date" ) AS [RowEndDate]
          FROM   #all_dates2
        ) as yy


        delete from #all_paired_dates2 where rowenddate is null or rowstartdate is null or rowstartdate=rowenddate

        IF OBJECT_ID('tempdb..#nearlythere') IS NOT NULL    
        DROP TABLE #nearlythere;

        select z.* into #nearlythere from (
        SELECT DISTINCT 
        /*Row Data*/
            DimIns.anlage 
            --,isnull(DimCon.vertrag,DimIns.vertrag) as vertrag /*sometimes due to data input error a contract exists in Installation table but not contract. In these cases we would not want to include the contract in the display*/
            ,DimCon.vertrag     
            ,isnull(DimCAc.[vkont],DimCon.vkonto) as vkonto     
            ,isnull(DimBPa.[partner],DimCAc.gpart) as gpart         
            ,CAST(CAST(CASE WHEN MESSIAH.RowStartDate=0 THEN '19800101'ELSE ISNULL(MESSIAH.RowStartDate,'19800101') END as varchar(8)) as date)     
                                        as [Row Start Date]
            ,CAST(CAST(CASE WHEN MESSIAH.RowEndDate=0 THEN '99991231'ELSE ISNULL(MESSIAH.RowEndDate,'99991231') END as varchar(8)) as date)         
                                        as [Row End Date]
            ,CASE WHEN  MESSIAH.RowEndDate<>'99991231' 
                THEN 0 ELSE CAST(


                CAST(ISNULL(DimIns.RowIsCurrent,1) as tinyint)
                *CAST(ISNULL(DimCon.RowIsCurrent,1) as tinyint)
                *CAST(ISNULL(DimCAc.RowIsCurrent,1) as tinyint)
                *CAST(ISNULL(DimBPa.RowIsCurrent,1) as tinyint)

                as bit) 
                END
                as [RowIsCurrent]
             ,1                         as [CountRow]



            --,ISNULL(DimIns.anlage,-942)   as [Installation]
             ,DimIns.anlage             as Installation
            ,DimIns.vstelle             as [Premise]
            ,DimCon.vertrag             as [Contract]
            ,DimCon.einzdat             as [Move in date]
            ,DimCon.auszdat             as [Move out date]
            ,DimIns.[RowDeletedFlag]    as [RowDeletedFlag_installation]
            ,DimCon.[RowDeletedFlag]    as [RowDeletedFlag_Contract]
            ,CASE WHEN isnull(DimCon.[RowDeletedFlag],0) =1 or isnull(DimIns.[RowDeletedFlag],0)=1 then 1 else 0 end as [RowDeletedFlag]
            ,DimCon.vkonto              as [Contract Account]
            ,DimBPa.[partner]           as [Business Partner]
            ,DimBPa.name_first          AS [First_Name]
            ,DimBPa.name_last           AS [Last_Name]
            ,DimBPa.name_org1           AS [Name1]
            ,DimBPa.name_org2           AS [Name2]

        /*Contract level*/
            ,DimCon.abrsperr            AS [Billing_Block]
            ,DimCon.bstatus             AS [Processing_Status]
            ,DimCon.kofiz               AS [Account_Determination]
            ,DimCon.sparte              AS [Division]
            ,DimCon.zzNEwCon            AS [New Connection (Contract)]
            ,DimCon.zzsource_code       AS [Source_Code]
            ,DimCac.[zzpartner]         AS [Source_Code_Partner]
        /*Contract Account level*/
             ,DimCAc.abwvk              AS [Coll_Bill_act]
             ,CASE
              WHEN DimCAc.ezawe = 'C' THEN 'Credit' 
              WHEN DimCAc.ezawe='D' THEN 'Direct Debit Variable' 
              WHEN DimCAc.ezawe='F' THEN 'Direct Debit Fixed' 
              WHEN DimCAc.ezawe='K' THEN 'Prepayment' 
              ELSE NULL
             END                        AS [Payment_Method]
             ,DimCAc.vkbez              AS [Contract_Name]
             ,DimCAc.zahlkond           AS [Payment_Terms]

        /*Business Partner level*/
            ,DimBPa.bpkind              AS [Type]

            ,DimBPa.zzecoappregi        AS [App_Reg]
            ,DimBPa.zzecoappvers        AS [App_Ver]
            ,DimBPa.zzzzemailinv        AS [Email_Invoice]
            ,DimBPa.birthdt             AS [D.O.B.]
            --,DimBPa.deathdt           


            ,CASE WHEN MESSIAH.RowStartDate=0 THEN '19800101'ELSE ISNULL(MESSIAH.RowStartDate,'19800101') END       
                                        as [RowStartDate]
            ,CASE WHEN MESSIAH.RowEndDate=0 THEN '99991231'ELSE ISNULL(MESSIAH.RowEndDate,'99991231') END           
                                        as [RowEndDate]





         FROM #all_paired_dates2 MESSIAH 


            LEFT JOIN bas.DimInstallation as DimIns
                ON MESSIAH.anlage=DimIns.anlage
                AND MESSIAH.vertrag=DimIns.vertrag
                AND MESSIAH.RowStartDate < DimIns.rowEndDate
                AND MESSIAH.RowEndDate > DimIns.RowStartDate 
    

            LEFT JOIN bas.DimContract as DimCon
                ON MESSIAH.vertrag=DimCon.vertrag
                AND MESSIAH.vkonto=Dimcon.vkonto
                AND MESSIAH.RowStartDate < DimCon.rowEndDate
                AND MESSIAH.RowEndDate > DimCon.RowStartDate  
                AND DimIns.anlage=DimCon.anlage

            LEFT JOIN bas.DimContractAccount as DimCAc
                ON MESSIAH.vkonto=DimCAc.vkont
                AND MESSIAH.gpart=DimCAc.gpart
                AND MESSIAH.RowStartDate < DimCAc.rowEndDate
                AND MESSIAH.RowEndDate > DimCAc.RowStartDate 

            LEFT JOIN bas.DimBusinessPartner as DimBPa
                ON MESSIAH.[GPart]=DimBPa.[partner]
                AND MESSIAH.RowStartDate < DimBPa.rowEndDate
                AND MESSIAH.RowEndDate > DimBPa.RowStartDate 



                where 
                dimins.anlage is not null /*there must be an installation or the entry is void*/
                and ((dimcon.vertrag is not null and dimcon.einzdat is not null) /*if a contract exists it must have a move in date or the entry is void*/
                    OR 
                    (dimcac.vkont is null and dimcon.einzdat is null) ) /*if a contract account exists but there is no move in date then the entry is void*/

        ) as z