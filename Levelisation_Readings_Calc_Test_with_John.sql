DECLARE @Inst_ID AS INT

SET @Inst_ID = 24870

;WITH Read_Dates
AS (SELECT S.INSTALLATION_ID_link,
           MAX(S.Meter_Read_Date) AS Start_Read_Date,
           T.End_Read_Date
    FROM dbo.READINGS_STAGING S
        INNER JOIN
        (
            SELECT INSTALLATION_ID_link,
                   MAX(Meter_Read_Date) AS End_Read_Date,
                   IncludedInLevelisation
            FROM dbo.READINGS_STAGING
            WHERE IncludedInLevelisation = 'FALSE'
                  AND INSTALLATION_ID_link = @Inst_ID
            GROUP BY INSTALLATION_ID_link,
                     IncludedInLevelisation
        ) T
            ON S.INSTALLATION_ID_link = T.INSTALLATION_ID_link
    WHERE S.IncludedInLevelisation = 'True'
          AND S.INSTALLATION_ID_link = @Inst_ID
    GROUP BY S.INSTALLATION_ID_link,
             S.IncludedInLevelisation,
             T.End_Read_Date)

SELECT I.FiT_ID_Initial,
       D.Start_Read_Date,
       D.End_Read_Date,
       R.Price

FROM dbo.INSTALLATION_STAGING I
    INNER JOIN Read_Dates D
        ON I.INSTALLATION_ID = D.INSTALLATION_ID_link
    Inner JOIN dbo.RATES R
        ON I.Initial_System_Tariff_Rate = R.Tariff_Code
           AND D.Start_Read_Date >= R.Valid_From
           AND D.End_Read_Date <= R.Valid_To;



