/*
Author:		Matthew Hollands
Date:		03/12/2018
Server:		d-ovs16-dwh-01
Database:	Levelisation

This copies all of the Rates from the Microtricity2 database to the Levelisation database.

*/


-- This Command clears down the Rates table in the Levelisation database.
TRUNCATE TABLE dbo.RATES;

-- Copy the Rates from the Microtricity database into the Levelisation database
INSERT INTO [dbo].[RATES]
(
  [RATE_ID]
      ,[Generation_Type]
      ,[Tariff_Code]
      ,[Description]
      ,[FiT_Period]
      ,[Min_Capacity]
      ,[Max_Capacity]
      ,[Valid_From]
      ,[Valid_To]
      ,[Price]
      ,[EligibilityDateFrom]
      ,[EligibilityDateTo]
      ,[ESP_Year]
      ,[Gross_Electricity_Charge]
)
SELECT [RATE_ID]
      ,[Generation_Type]
      ,[Tariff_Code]
      ,[Description]
      ,[FiT_Period]
      ,[Min_Capacity]
      ,[Max_Capacity]
      ,[Valid_From]
      ,[Valid_To]
      ,[Price]
      ,[EligibilityDateFrom]
      ,[EligibilityDateTo]
      ,[ESP_Year]
      ,[Gross_Electricity_Charge]

FROM [UH-GENDB-01].[Microtricity2].[dbo].[RATES] 
