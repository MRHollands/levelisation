/*
Author:		Matthew Hollands
Date:		03/12/2018
Server:		d-ovs16-dwh-01
Database:	Levelisation

This copies all of the installation details from the Microtricity2 database to the Levelisation database.

*/


-- This Command clears down the Installation_Proc table in the Levelisation database.  This is not currently working as RPC is not turned on on d-ovs
TRUNCATE TABLE dbo.INSTALLATION_STAGING;

-- Copy the Readings from the Microtricity database into the Levelisation database
INSERT INTO INSTALLATION_STAGING
(
    INSTALLATION_ID
      ,Scheme_Type
      ,Generation_Type
      ,Total_Installed_Capacity_kW
      ,FiT_ID_Initial
      ,Initial_System_Tariff_Rate
      ,Initial_System_Export_Rate
      ,Initial_System_Pct_Split
      ,FiT_ID_Ext1
      ,Extension_1_Tariff_Rate
      ,Extension_1_Export_Rate
      ,Extension_1_Pct_Split
      ,FiT_ID_Ext2
      ,Extension_2_Tariff_Rate
      ,Extension_2_Export_Rate
      ,Extension_2_Pct_Split
      ,TopUp_Tariff_Rate
      ,ExpPct1
      ,ExpPct2
      ,InitSysExpRate2
      ,Account_Status
)
SELECT  INSTALLATION_ID
      ,Scheme_Type
      ,Generation_Type
      ,Total_Installed_Capacity_kW
      ,FiT_ID_Initial
      ,Initial_System_Tariff_Rate
      ,Initial_System_Export_Rate
      ,Initial_System_Pct_Split
      ,FiT_ID_Ext1
      ,Extension_1_Tariff_Rate
      ,Extension_1_Export_Rate
      ,Extension_1_Pct_Split
      ,FiT_ID_Ext2
      ,Extension_2_Tariff_Rate
      ,Extension_2_Export_Rate
      ,Extension_2_Pct_Split
      ,TopUp_Tariff_Rate
      ,ExpPct1
      ,ExpPct2
      ,InitSysExpRate2
      ,Account_Status
FROM [UH-GENDB-01].[Microtricity2].[dbo].[Installation] I INNER JOIN [UH-GENDB-01].[Microtricity2].[dbo].[Contract] C 
ON I.INSTALLATION_ID = C.INSTALLATION_ID_link