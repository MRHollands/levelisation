WITH
  E
AS
(
  SELECT Installation_ID, Number_of_Splits +1 AS Number_of_Splits FROM Levelisation_Calcs_vw  -- Adding 1 to the Number of Splits because on the first pass the Union Below subtracts 1.

UNION ALL

  SELECT Installation_ID, Number_of_Splits - 1 FROM E WHERE Number_of_Splits > 1
)

SELECT
  I.FiT_ID_Initial,Number_of_splits
  --,ROW_NUMBER() OVER (ORDER BY Fit_ID_Initial, Number_of_Splits) AS unique_ref
FROM
 dbo.INSTALLATION_STAGING I INNER JOIN E  ON I.INSTALLATION_ID = E.Installation_ID
 WHERE Number_of_Splits >=1
ORDER BY
  I.INSTALLATION_ID,
    Number_of_Splits